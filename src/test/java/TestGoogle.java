import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestGoogle {

    @Test
    public void testDTF() {
        // Driver Initialization
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("no-sandbox");
        options.merge(capabilities);
        WebDriver driver = new ChromeDriver(options);

        // Homework code
        System.out.println("Automized DTF-Test-3");

        // Check initial page title
        driver.get("https://dtf.ru/");
        String mainPageTitle = "DTF — игры, кино, сериалы, разработка, сообщество";

        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals(mainPageTitle, driver.getTitle());

        // Open the first post in the list (and memorize its title)
        WebElement firstPost = driver.findElement(By.xpath("//*[@id=\"page_wrapper\"]/div/div[6]/div[1]/div/div[1]"));
        String postTitle = firstPost.findElement(By.xpath("div/div[2]/div/div[1]")).getText();
        String blogTitle = firstPost.findElement(By.xpath("div/div[1]/div[1]/a[1]/div[2]")).getText();
        System.out.println("Post Title is " + postTitle);
        firstPost.click();

        // Check that correct post was opened by comparing memorized title with opened post title
        driver.get(driver.getCurrentUrl()); // Update driver location (for some reason it otherwise look at root)
        String openedPostTitle = driver.findElement(By.xpath("//*[@id=\"page_wrapper\"]/div[2]/div[1]/div[1]/h1")).getText();
        Assert.assertEquals(postTitle, openedPostTitle);

        // Page title is composed of post title and blog title
        Assert.assertEquals(postTitle + " — " + blogTitle + " на DTF", driver.getTitle());

        // Return to main page
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/a")).click();
        driver.get(driver.getCurrentUrl());
        Assert.assertEquals(mainPageTitle, driver.getTitle());

        driver.quit();

        // Lab Code
        /*driver.get("http://www.google.com");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Google", driver.getTitle());

        WebElement findLine = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input"));
        findLine.sendKeys("SQR course is the best");

        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[3]/center/input[1]"));
        searchButton.click();

        findLine = driver.findElement(By.xpath("/html/body/div[4]/form/div[2]/div[1]/div[2]/div/div[2]/input"));
        Assert.assertEquals("SQR course is the best", findLine.getAttribute("value"));

        driver.quit();*/
    }
}
